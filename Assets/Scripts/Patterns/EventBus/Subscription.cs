﻿using System;
using HelpyPatterns.Events;
using HelpyPatterns.Interfaces;

namespace HelpyPatterns
{
    internal class Subscription<T> : ISubscription where T : EventBase { // {
        public object SubscriptionToken { get { return _action; } }

        private readonly Action<T> _action;

        public Subscription(Action<T> action) {
            if(action == null)
                throw new ArgumentNullException("action");

            _action = action;
//            _subscriptionToken = token;
        }

        public void Publish(T eventItem) {
            if (!(eventItem is EventBase))
                 throw new ArgumentException("Event Item is not the correct type.");

            _action.Invoke(eventItem);
        }

        
//        private readonly SubscriptionToken _subscriptionToken;
    }
}
