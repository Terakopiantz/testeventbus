﻿using System;
using HelpyPatterns.Events;

namespace HelpyPatterns.Interfaces {
    public interface ISubscription {
        /// <summary>
        /// Token returned to the subscriber
        /// </summary>
        object SubscriptionToken { get; }

        /// <summary>
        /// Publish to the subscriber
        /// </summary>
        /// <param name="eventBase"></param>
        //void Publish<T>(T eventBase) where T : EventBase;
    }
}