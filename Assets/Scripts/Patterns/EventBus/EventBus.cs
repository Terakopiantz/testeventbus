﻿using UnityEngine.Events;
using System.Collections.Generic;
using HelpyPatterns.Interfaces;
using System;
using HelpyPatterns.Events;
using System.Linq;

namespace HelpyPatterns {
    public class EventBus : Singleton<EventBus>, IEventBus 
    { 
        private readonly IDictionary<Type, List<ISubscription>> _subscriptions;
        private readonly Dictionary<object, Type> typeMap;
        private static readonly object SubscriptionsLock = new object();
        
        public override void Awake() {
            base.Awake();
        }
        

        public EventBus() {
            this._subscriptions = new Dictionary<Type, List<ISubscription>>();
            this.typeMap = new Dictionary<object, Type>();
        }

        /// <summary>
        /// Subscribes to the specified event type with the specified action
        /// </summary>
        /// <typeparam name="TEventBase">The type of event</typeparam>
        /// <param name="action">The Action to invoke when an event of this type is published</param>
        /// <returns>A <see cref="SubscriptionToken"/> to be used when calling <see cref="Unsubscribe"/></returns>
        public void Subscribe<T>(Action<T> action) where T : EventBase {
            if (action == null)
                throw new ArgumentNullException("action");
           
            lock (SubscriptionsLock) {
                if (!this._subscriptions.ContainsKey(typeof(T)))
                    this._subscriptions.Add(typeof(T), new List<ISubscription>());

                this.typeMap.Add(action, typeof(T));
                this._subscriptions[typeof(T)].Add(new Subscription<T>(action));
            }
        }

        /// <summary>
        /// Unsubscribe from the Event type related to the specified <see cref="SubscriptionToken"/>
        /// </summary>
        /// <param name="token">The <see cref="SubscriptionToken"/> received from calling the Subscribe method</param>
        public void Unsubscribe<T>(Action<T> action) where T : EventBase {
            if (action == null)
                throw new ArgumentNullException("action");

            lock (SubscriptionsLock) {
                Type type = this.typeMap[action];
                if (this._subscriptions.ContainsKey(type)) {
                    var allSubscriptions = this._subscriptions[type];
                    var subscriptionToRemove = allSubscriptions.FirstOrDefault(x => ReferenceEquals(x.SubscriptionToken, action));
                    if (subscriptionToRemove != null)
                        this._subscriptions[type].Remove(subscriptionToRemove);
                }
            }
        }

        /// <summary>
        /// Publishes the specified event to any subscribers for the <see cref="TEventBase"/> event type
        /// </summary>
        /// <typeparam name="TEventBase">The type of event</typeparam>
        /// <param name="eventItem">Event to publish</param>
        public void Publish<T>(T eventItem) where T : EventBase {
            if (eventItem == null)
                throw new ArgumentNullException("eventItem");

            List<Subscription<T>> allSubscriptions = new List<Subscription<T>>();
            lock (SubscriptionsLock) {
                if (_subscriptions.ContainsKey(typeof(T))) {
                    foreach(ISubscription iSub in _subscriptions[typeof(T)]) {
                        allSubscriptions.Add((Subscription<T>)iSub);
                    }
                }
            }

            foreach (var subscription in allSubscriptions) {
                subscription.Publish(eventItem);
            }
        }
    }
}

/*

public class HelpyEventBus : Singleton<HelpyEventBus>
{
    public enum Events {
        LeftButtonClicked,
        RightButtonClicked
    }


    private Dictionary<Events, HelpyEvent> m_EventDictionary;

    public override void Awake() {
        base.Awake();
        Instance.Init();
    }

    private void Init() {
        if (Instance.m_EventDictionary == null) {
            Instance.m_EventDictionary = new Dictionary<Events, HelpyEvent>();
        }
    }

    public static void StartListening(Events eventName, UnityAction<EventInfos> listener) {
        HelpyEvent thisEvent = null;
        if (Instance.m_EventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.AddListener(listener);
        } else {
            thisEvent = new HelpyEvent();
            thisEvent.AddListener(listener);
            Instance.m_EventDictionary.Add(eventName, thisEvent);
        }
    }

    public static void StopListening(Events eventName, UnityAction<EventInfos> listener) {
        HelpyEvent thisEvent = null;
        if (Instance.m_EventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(Events eventName, EventInfos eventInfos) {
        HelpyEvent thisEvent = null;
        if (Instance.m_EventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.Invoke(eventInfos);
        }
    }
}
*/