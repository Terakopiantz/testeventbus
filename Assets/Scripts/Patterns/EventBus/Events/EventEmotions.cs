﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HelpyPatterns.Events 
{
    public class ClickButtonEvent : EventBase {
        public string stringAttribute;
        public Color color;

        public ClickButtonEvent(string myString, Color color) {
            this.stringAttribute = myString;
            this.color = color;
        }
    }

    public class OtherEvent : EventBase {
        public string blublub;
        public string sefsf;
        public int cace;
        public Color color;

        public OtherEvent(string myString, Color color) {
            this.blublub = myString;
            this.sefsf = "ndb";
            this.cace = 8;
            this.color = color;
        }
    }
}

