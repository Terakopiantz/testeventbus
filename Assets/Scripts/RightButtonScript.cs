﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HelpyPatterns.Events;
using HelpyPatterns;

public class RightButtonScript : MonoBehaviour
{
    public EventBus Bus;

    private void Awake() { Bus = EventBus.Instance; }

    // Update is called once per frame
    public void Click() {
        Bus.Publish(new ClickButtonEvent("bouton DROIT", Color.blue));
    }


}




