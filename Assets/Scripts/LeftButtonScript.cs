﻿using HelpyPatterns;
using HelpyPatterns.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftButtonScript : MonoBehaviour
{
    public EventBus Bus;

    private void Awake() { Bus = EventBus.Instance; }

    // Update is called once per frame
    public void Click() {
        Bus.Publish(new ClickButtonEvent("bouton GAUCHE", Color.red));
    }
}
