﻿using HelpyPatterns;
using HelpyPatterns.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LabelScript : MonoBehaviour
{
    public EventBus Bus;

    private void Awake() { Bus = EventBus.Instance; }

    void Start() {
        Bus.Subscribe<ClickButtonEvent>(OnClickButton);
    }

    void OnClickButton(ClickButtonEvent myEvent) {
        GetComponent<Text>().text = myEvent.stringAttribute;
        GetComponent<Text>().color = myEvent.color;
    }

    void OnDestroy() {
        Bus.Unsubscribe<ClickButtonEvent>(OnClickButton);
    }

    /*
    private bool m_IsQuitting;

    void OnEnable() {
        HelpyEventBus.StartListening(HelpyEventBus.Events.LeftButtonClicked, OnLeftButtonTrigger);
        HelpyEventBus.StartListening(HelpyEventBus.Events.RightButtonClicked, OnRightButtonTrigger);
    }

    void OnApplicationQuit() {
        m_IsQuitting = true;
    }

    void OnDisable() {
        if (m_IsQuitting == false) {
            HelpyEventBus.StopListening(HelpyEventBus.Events.LeftButtonClicked, OnLeftButtonTrigger);
            HelpyEventBus.StopListening(HelpyEventBus.Events.RightButtonClicked, OnRightButtonTrigger);
        }
    }

    public void OnLeftButtonTrigger(EventInfos eventInfos) {
        LeftButtonClickedEventInfos infos = null;
        try {
            infos = (LeftButtonClickedEventInfos)eventInfos;
        } finally {
            GetComponent<Text>().text = infos.label;
            GetComponent<Text>().color = infos.color;
        }
    }

    public void OnRightButtonTrigger(EventInfos infos) {
        RightButtonClickedEventInfos infoCasted = (RightButtonClickedEventInfos)infos;
        GetComponent<Text>().text = infoCasted.label;
        GetComponent<Text>().color = infoCasted.color;
    }
    */
}
